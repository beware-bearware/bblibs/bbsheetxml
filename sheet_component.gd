# A sheet component is some concept which effects the layout of the
# character sheet physically.
class_name bbSheetComponent

var name: String 
var parent: bbSheetComponent
var sub_components: Array[bbSheetComponent]

func new() -> bbSheetComponent:
	parent = null
	var name_chars = "abcdefghijklmnopqrstuvwxyz"
	for x in range(8):
		name = name + name_chars[randi() % len(name_chars)]
	return self

func add_sub_component(target_component: bbSheetComponent) -> void:
	target_component.parent = self
	sub_components.append(target_component)
	return

func remove_sub_component(target_component: bbSheetComponent) -> void:
	target_component.parent = null
	sub_components.erase(target_component)

func get_sub_components() -> Array[bbSheetComponent]:
	return sub_components

func get_parent_component() -> bbSheetComponent:
	return parent
