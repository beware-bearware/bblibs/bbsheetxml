class_name bbSheet


var root_component: bbSheetComponent
var size: Vector2 = Vector2(850, 1100)

func new() -> bbSheet:
	root_component = bbSheetComponent.new()
	root_component.name = "root"
	return self
