# bbSheetXML
! bbSheetXML IS NOT READY FOR GENERAL USE !
This library provides a set of functions to let developers make, process, and use Bearware Sheet style XML files, or bsxml. Bsxml files are based on the XML syntax, only adding a specific set of standard tags with intended meaning between all projects which use it on top of the existing syntax. The meaning for these tags defines the specific layout and in-built logic for a Bearware Character Sheet, or essentially a Table Top Roleplaying Game character sheet with some in-built pre-defined logic. This format is free to use, along with this library, with no strings attached, as is the format used and exported by all Bearware projects which use character sheets.

## Installation
If you don't already have one, create an addons directory in your Godot project, and make sure another directory inside of it named "bbLibs" is present. Open this directory, and clone this repository inside.
```bash
git clone https://gitlab.com/beware-bearware/bblibs/SheetXML.git
```
Once cloned, go into your project's autoload settings and set the bbSheetXML.gd file within the cloned folder as the path, and bbSheetXML as the name.

## Usage
Due to being autoloaded, the script can be used by any other script by using the bbSheetXML class. More documentation will be arriving once the project is more usable.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)