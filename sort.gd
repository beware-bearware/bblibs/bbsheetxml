# A sort component defines the relative position of sub components within
# itself on a scale of its area. For example, a 900unit long sort component
# with a 25% and 70% split would put its first child component in the
# 0-225 unit area, the second in the 225-630 unit area, and the last
# in the 630-900 unit area.
class_name bbSheetSortComponent extends bbSheetComponent

var splits: Array[int]
